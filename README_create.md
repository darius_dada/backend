docker run -p 8000:8000 amazon/dynamodb-local

aws dynamodb create-table \
    --table-name ecarisaj-dogs \
    --attribute-definitions \
        AttributeName=ID,AttributeType=S \
    --key-schema \
        AttributeName=ID,KeyType=HASH \
--provisioned-throughput \
        ReadCapacityUnits=10,WriteCapacityUnits=5 \
--endpoint-url http://localhost:8000