import json
import boto3
import pprint
import base64
import uuid

S3_BUCKET_NAME = 'licenta.dariuscernea.me'
S3_BUCKET_IMAGE_PATH = 'images/dogs'
DYNAMO_TABLE_NAME = 'ecarisaj-dogs'

s3 = boto3.client('s3')
pp = pprint.PrettyPrinter(indent=4)
dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table(DYNAMO_TABLE_NAME)


def lambda_handler(event, context):
    if event.get('httpMethod') == 'OPTIONS':
        return {
            'statusCode': 200,
            'headers': {
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Headers': '*',
                'Access-Control-Allow-Methods': '*'
            },
            'body': json.dumps({'message': 'CORS enabled'})
        }
    response = 405
    if event.get('httpMethod') == 'DELETE':
        response = handle_delete_event(event)

    if event.get('httpMethod') == 'POST':
        response = handle_post_event(event)

    if event.get('httpMethod') == 'PATCH':
        response = handle_patch_event(event)

    return {"statusCode": response,
            "body": json.dumps({"message": "Status code from AWS services"}),
            "headers": {"Access-Control-Allow-Origin": "*", "Access-Control-Allow-Headers": "*"}}


def handle_patch_event(event):
    dog_id = event.get('pathParameters')['dog']
    decoded_body = json.loads(base64.b64decode(event['body']))

    update_response = table.update_item(
        Key={'ID': dog_id},
        UpdateExpression="set dog_name= :n , description=:d",
        ExpressionAttributeValues={
            ':n': decoded_body['dog_name'],
            ':d': decoded_body['description']
        },
        ReturnValues="UPDATED_NEW"
    )
    return update_response['ResponseMetadata']['HTTPStatusCode']


def handle_delete_event(event):
    dog_id = event.get('pathParameters')['dog']
    delete_response = table.delete_item(Key={'ID': dog_id})
    return delete_response['ResponseMetadata']['HTTPStatusCode']


def handle_post_event(event):
    decoded_body = json.loads(base64.b64decode(event['body']))

    image_name = decoded_body['image_name']
    image = decoded_body['image']
    image = image[image.find(",") + 1:]
    dec = base64.b64decode(image + "===")
    upload_response = s3.put_object(Bucket=S3_BUCKET_NAME, Key='{}/{}'.format(S3_BUCKET_IMAGE_PATH, image_name),
                                    Body=dec)

    if upload_response['ResponseMetadata']['HTTPStatusCode'] != 200:
        return upload_response['ResponseMetadata']['HTTPStatusCode']

    location = s3.get_bucket_location(Bucket=S3_BUCKET_NAME)['LocationConstraint']
    url = "http://{}.s3-website.{}.amazonaws.com/{}/{}".format(S3_BUCKET_NAME, location, S3_BUCKET_IMAGE_PATH,
                                                               image_name)
    dynamo_entry = {'ID': uuid.uuid1().hex,
                    'description': decoded_body['description'],
                    'dog_name': decoded_body['dog_name'],
                    'image_url': url}

    update_dynamo_response = table.put_item(Item=dynamo_entry)
    return update_dynamo_response['ResponseMetadata']['HTTPStatusCode']
