import json
import boto3
import pprint

pp = pprint.PrettyPrinter(indent=4)

DYNAMO_TABLE = 'ecarisaj-dogs'
dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table(DYNAMO_TABLE)


def lambda_handler(event, context):
    if event.get('httpMethod') == 'OPTIONS':
        return {
            'statusCode': 200,
            'headers': {
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Headers': '*',
                'Access-Control-Allow-Methods': '*'
            },
            'body': json.dumps({'message': 'CORS enabled'})
        }
    
    if event.get('pathParameters', False):
        dog_id = event.get('pathParameters')['dog']
        response = table.get_item(Key={'ID': dog_id})
        response_data = response.get('Item')
    else:
        response = table.scan()
        response_data = response.get('Items')

    return {
        "statusCode": response['ResponseMetadata']['HTTPStatusCode'],
        "body": json.dumps(response_data),
        "headers": {"Access-Control-Allow-Origin": "*", "Access-Control-Allow-Headers": "*"}
    }
